// Copyright 2016 Google Inc. All rights reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package gitindex

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"sort"

	"github.com/pkg/errors"
)

// CloneRepo clones one repository, adding the given config
// settings. It returns the bare repo directory. The `name` argument
// determines where the repo is stored relative to `destDir`. Returns
// the directory of the repository.
func CloneRepo(destDir, name, cloneURL string, settings map[string]string) (string, error) {
	parent := filepath.Join(destDir, filepath.Dir(name))
	if err := os.MkdirAll(parent, 0755); err != nil {
		return "", err
	}

	repoDest := filepath.Join(parent, filepath.Base(name)+".git")
	if _, err := os.Lstat(repoDest); err == nil {
		if isValidGitDir(repoDest) {
			return "", nil
		}
		// It's not a valid git directory so remove it.
		if err := os.RemoveAll(repoDest); err != nil {
			return "", errors.Wrapf(err, "removing invalid git directory %s", repoDest)
		}
	}

	var keys []string
	for k := range settings {
		keys = append(keys, k)
	}
	sort.Strings(keys)

	var config []string
	for _, k := range keys {
		if settings[k] != "" {
			config = append(config, "--config", k+"="+settings[k])
		}
	}

	tmpDir, err := ioutil.TempDir(filepath.Dir(repoDest), "repo")
	if err != nil {
		return "", errors.Wrap(err, "making temp dir for repo")
	}

	cmd := exec.Command(
		"git", "clone", "--bare", "--verbose", "--progress",
	)
	cmd.Args = append(cmd.Args, config...)
	cmd.Args = append(cmd.Args, cloneURL, tmpDir)

	// Prevent prompting
	outBuf := &bytes.Buffer{}
	errBuf := &bytes.Buffer{}
	cmd.Stdout = outBuf
	cmd.Stderr = errBuf
	log.Printf("running: %v", cmd.Args)
	if err := cmd.Run(); err != nil {
		return "", fmt.Errorf("command %s failed: %v\nOUT: %s\nERR: %s", cmd.Args, err, outBuf.String(), errBuf.String())
	}

	if err := os.Rename(tmpDir, repoDest); err != nil {
		return "", errors.Wrap(err, "moving temp location for git cloned dir to its final destination")
	}

	return repoDest, nil
}

func isValidGitDir(dir string) bool {
	cmd := exec.Command("git", "log")
	cmd.Dir = dir
	return cmd.Run() == nil
}
