// Copyright 2016 Google Inc. All rights reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package web

import (
	"html/template"
	"log"
)

// Top provides the standard templates in parsed form
var Top = template.New("top").Funcs(Funcmap)

// TemplateText contains the text of the standard templates.
var TemplateText = map[string]string{

	"didyoumean": `
<html>
<head>
	<title>Error</title>
</head>
<body>
	<p>{{.Message}}. Did you mean <a href="/search?q={{.Suggestion}}">{{.Suggestion}}</a> ?
</body>
</html>
`,

	"head": `
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" href="data:image/x-icon;base64,AAABAAEAKCgAAAEAIABoGgAAFgAAACgAAAAoAAAAUAAAAAEAIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wr///9I////fP///57////F////zP///8z////G////oP///37///9K////Df///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////Uf///7L////5//////////////////////////////////////////////////////////z///+1////Vv///wL///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///9d////2v/////////////////////////////////////////////////////////////////////////////////////////g////Yv///wH///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///yX////E///////////////////////////////////////////////////////////////////////////////////////////////////////////////M////K////wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///1f////0///////////////////////////////////////////w8PD/5eXl/9LS0v/Q0ND/4+Pj/+3t7f////////////////////////////////////////////////X///9g////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///2//////////////////////////////////////2tra/5+fn/9TU1P/AAAA/wAAAP8AAAD/AAAA/wAAAP8AAAD/Q0ND/5eXl//T09P//Pz8/////////////////////////////////////4P///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///3D////////////////////////////////R0dH/aWlp/wAAAP8AAAD/AAAA/wAAAP8AAAD/AAAA/wAAAP8AAAD/AAAA/wAAAP8AAAD/AAAA/1BQUP/Gxsb//f39////////////////////////////////hP///wD///8A////AP///wD///8A////AP///wD///8A////AP///1b///////////////////////////T09P+MjIz/AAAA/wAAAP8AAAD/AAAA/wAAAP8lJSX/enp6/46Ojv+Tk5P/e3t7/z09Pf8AAAD/AAAA/wAAAP8AAAD/AAAA/3Nzc//q6ur///////////////////////////////9i////AP///wD///8A////AP///wD///8A////AP///yX////0///////////////////////////7+/v/h4eH/wAAAP8AAAD/PT09/6urq//f39//////////////////////////////////5+fn/7a2tv9RUVH/AAAA/wAAAP8AAAD/ODg4/9ra2v//////////////////////////9f///y3///8A////AP///wD///8A////AP///wD////E//////////////////////////////////////r6+v+Dg4P/qamp//f39///////////////////////////////////////////////////////+/v7/7q6uv8oKCj/AAAA/wAAAP8fHx//19fX///////////////////////////P////Av///wD///8A////AP///wD///9c//////////////////////Ly8v/8/Pz//////////////////v7+////////////////////////////////////////////////////////////////////////////5ubm/19fX/8AAAD/AAAA/zY2Nv/l5eX//////////////////////////2f///8A////AP///wD///8B////2f////////////////////+IiIj/pKSk///////////////////////////////////////////////////////////////////////////////////////////////////////x8fH/YWFh/wAAAP8AAAD/ZWVl//r6+v/////////////////////m////Bv///wD///8A////UP/////////////////////Ozs7/AAAA/wAAAP+rq6v//////////////////////////////////////////////////////////////////////////////////////////////////////+vr6/83Nzf/AAAA/wAAAP+2trb//////////////////////////1z///8A////AP///7H////////////////+/v7/YWFh/wAAAP8AAAD/ra2t////////////////////////////////////////////////////////////////////////////////////////////////////////////xcXF/wAAAP8AAAD/OTk5//b29v////////////////////+6////AP///wr////5////////////////1tbW/wAAAP8AAAD/RUVF//j4+P////////////////////////////////////////////////////////////////////////////////////////////////////////////7+/v9wcHD/AAAA/wAAAP+/v7///////////////////////v///xL///9H/////////////////////5iYmP8AAAD/AAAA/7Kysv//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////ysrK/wAAAP8AAAD/dHR0//////////////////////////9S////e/////////////////////9DQ0P/AAAA/wAAAP/m5ub///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////f39/8GBgb/AAAA/wAAAP/z8/P/////////////////////iP///57////////////////r6+v/AAAA/wAAAP9HR0f/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////fHx8/wAAAP8AAAD/2NjY/////////////////////63////F////////////////2dnZ/wAAAP8AAAD/f39//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////6mpqf8AAAD/AAAA/8rKyv/////////////////////M////zP///////////////8vLy/8AAAD/AAAA/6enp/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////+2trb/AAAA/wAAAP+xsbH/////////////////////zP///8z////////////////Ly8v/AAAA/wAAAP+qqqr/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////vr6+/wAAAP8AAAD/ra2t/////////////////////8z////H////////////////0dHR/wAAAP8AAAD/ioqK/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////6qqqv8AAAD/AAAA/8XFxf/////////////////////M////of///////////////+fn5/8AAAD/AAAA/2JiYv////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////+NjY3/AAAA/wAAAP/Pz8//////////////////////sv///4D////////////////8/Pz/CwsL/wAAAP8AAAD/8vLy///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////9/f3/QEBA/wAAAP8AAAD/7Ozs/////////////////////47///9M/////////////////////4iIiP8AAAD/AAAA/8PDw///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////2tra/wAAAP8AAAD/VFRU//////////////////////////9X////D/////3////////////////FxcX/AAAA/wAAAP9sbGz//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////5CQkP8AAAD/AAAA/6ysrP//////////////////////////GP///wD///+3////////////////9/f3/zk5Of8AAAD/AAAA/8vLy////////////////////////////////////////////////////////////////////////////////////////////////////////////+Dg4P8AAAD/AAAA/wAAAP/p6en/////////////////////xf///wD///8A////Wv////////////////////+zs7P/AAAA/wAAAP9BQUH/8fHx//////////////////////////////////////////////////////////////////////////////////////////////////r6+v9nZ2f/AAAA/wAAAP+YmJj//////////////////////////2b///8A////AP///wX////l////////////////+fn5/1paWv8AAAD/AAAA/3p6ev/6+vr////////////////////////////////////////////////////////////////////////////////////////////X19f/AAAA/wAAAP85OTn/7u7u/////////////////////+z///8L////AP///wD///8A////aP/////////////////////e3t7/AwMD/wAAAP8AAAD/gYGB//Pz8////////////////////////////////////////////////////////////////////////////////////////////8jIyP8AAAD/ysrK//////////////////////////9z////AP///wD///8A////AP///wL////S/////////////////////8nJyf8AAAD/AAAA/wAAAP9XV1f/2tra//////////////////////////////////////////////////////////////////v7+///////////////////////2tra///////////////////////////Z////CP///wD///8A////AP///wD///8A////MP////j/////////////////////ycnJ/wAAAP8AAAD/AAAA/wAAAP+JiYn/29vb////////////////////////////////////////////4uLi/6Ghof9bW1v/8fHx///////////////////////////////////////////+////Ov///wD///8A////AP///wD///8A////AP///wD///9p///////////////////////////W1tb/QkJC/wAAAP8AAAD/AAAA/wAAAP9MTEz/l5eX/7i4uP/Ly8v/y8vL/7y8vP+enp7/WVlZ/wAAAP8AAAD/AAAA/2FhYf/y8vL/////////////////////////////////////dv///wD///8A////AP///wD///8A////AP///wD///8A////AP///5T///////////////////////////Pz8/+goKD/AAAA/wAAAP8AAAD/AAAA/wAAAP8AAAD/AAAA/wAAAP8AAAD/AAAA/wAAAP8AAAD/AAAA/wAAAP+Li4v/7e3t////////////////////////////////mv///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////l////////////////////////////////+vr6/+rq6v/Tk5O/wAAAP8AAAD/AAAA/wAAAP8AAAD/AAAA/wAAAP8AAAD/Ozs7/5+fn//i4uL/////////////////////////////////////mv///wH///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///9t////+//////////////////////////////////////l5eX/ysrK/62trf+qqqr/qqqq/6urq//Hx8f/4ODg//39/f/////////////////////////////////////+////ev///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///zb////Y///////////////////////////////////////////////////////////////////////////////////////////////////////////////c////P////wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////B////3P////s/////////////////////////////////////////////////////////////////////////////////////////+////95////Cv///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////C////2r////K////////////////////////////////////////////////////////////////////zv///2////8N////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///xz///9e////lf///8H////O////4v///+P////P////w////5f///9i////Hv///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A//wAP/8AAAD/8AAH/wAAAP/AAAH/AAAA/wAAAP8AAAD+AAAAfwAAAPwAAAA/AAAA+AAAAB8AAADwAAAADwAAAOAAAAAHAAAA4AAAAAMAAADAAAAAAwAAAIAAAAABAAAAgAAAAAEAAACAAAAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIAAAAABAAAAgAAAAAEAAACAAAAAAQAAAMAAAAADAAAAwAAAAAMAAADgAAAABwAAAPAAAAAPAAAA+AAAAB8AAAD8AAAAHwAAAP4AAAB/AAAA/wAAAP8AAAD/gAAB/wAAAP/gAAf/AAAA//wAP/8AAAA=" />
<style>
	@font-face {
		font-family: 'Cabin';
		font-style: normal;
		font-weight: 400;
		font-display: swap;
		src: local('Cabin'), local('Cabin-Regular'), url(https://fonts.gstatic.com/s/cabin/v14/u-4x0qWljRw-Pd8w__g.ttf) format('truetype');
	}
	#navsearchbox { width: 350px !important; }
	#maxhits { width: 100px !important; }
	.container {
		background-color: white;
	}
	.navbar-default {
		 background-color: white;
		 border: 0;
	}
	.input-group-addon {
		 background-color: white;
	}
	.label-dup {
		border-width: 1px !important;
		border-style: solid !important;
		border-color: #aaa !important;
		color: black;
	}
	.noselect {
		user-select: none;
	}
	a.label-dup:hover {
		color: black;
		background: #ddd;
	}
	.result {
		display: block;
		content: " ";
		visibility: hidden;
	}
	.container-results {
		 overflow: auto;
		 max-height: calc(100% - 72px);
	}
	.inline-pre {
		 border: unset;
		 background-color: unset;
		 margin: unset;
		 padding: unset;
		 overflow: unset;
		 font-family: monospace, Menlo, Monaco, Consolas, "Courier New";
	}
	:target { background-color: #ccf; }
	td { border: none !important; padding: 2px !important; }

	body {
		background-color: white;
		font-family: Arial, "Helvetica Neue", Helvetica, sans-serif;
		margin: 1em;
	}

	button {
		border-radius: 0.3em;
		padding-left: 1.2em;
		padding-right: 1.2em;
		font-size: 17px;
		border-color: #800080;
		background-color: #933f93;
		color: white;
		cursor: pointer;
	}

	button:hover {
		background-color: #933f937d;
	}

	input {
		border-color: #800080;
		border-radius: 0.3em;
		border-width: 1px;
		display: block;
		padding: 1em;
		width: 100%;
		font-size: 17px;
	}

	input:focus { outline-style: none; }

	a {
		color: rgb(51, 122, 183);
		text-decoration: none;
		margin-top: 1em;
	}

	</style>
</head>
	`,

	"jsdep": `
`,

	// the template for the search box.
	"searchbox": `
<div style="font-size:4em; text-align:center; font-family:'Cabin',sans-serif; color: #800080;">ZeroSearch</div>
<form action="search" style="display:grid; grid-template-columns: 1fr auto; grid-gap:0.5em">
	<input type="text" id="searchbox" name="q" autofocus
		{{if .Query}}
			value={{.Query}}
		{{end}}
		>
	<button>Go</button>
</form>
`,

	"navbar": `
<form action="search" style="display:grid; grid-template-columns: 1fr auto; grid-gap:0.5em">
	<div style="font-size:1.5em; font-family:'Cabin',sans-serif; text-align:center; color: #800080;">ZeroSearch</div>
	<div></div>
	<input size="1" type="text" id="searchbox" name="q" autofocus style="width:auto; display:inline"
		{{if .Query}}
			value={{.Query}}
		{{end}}
		>
	<button>Go</button>
</form>
`,
	// search box for the entry page.
	"search": `
<html>
	{{template "head"}}
	<title>ZeroSearch</title>
	<body>
		<div><a href="/about">About</a></div>
		{{template "searchbox" .Last}}
	</body>
</html>
`,
	"results": `
<html>
{{template "head"}}
<title>{{.QueryStr}} - ZeroSearch</title>
<body id="results">
	{{template "navbar" .Last}}
	<div class="container-fluid container-results">
		<div style="margin-top:0.5em">
			{{if .Stats.Crashes}}<br><b>{{.Stats.Crashes}} shards crashed</b><br>{{end}}
			{{ $fileCount := len .FileMatches }}
			Found {{.Stats.MatchCount}} results in {{.Stats.FileCount}} files{{if or (lt $fileCount .Stats.FileCount) (or (gt .Stats.ShardsSkipped 0) (gt .Stats.FilesSkipped 0)) }},
				showing top {{ $fileCount }} files (<a rel="nofollow"
					 href="search?q={{.Last.Query}}&num={{More .Last.Num}}">show more</a>).
			{{else}}.{{end}}
		</div>
		{{range .FileMatches}}
		<table>
			<thead>
				<tr>
					<th colspan="2" style="text-align:left">
						{{if .URL}}<a name="{{.ResultID}}" class="result"></a><a href="{{.URL}}" >{{else}}<a name="{{.ResultID}}">{{end}}
						{{.Repo}}:{{.FileName}}</a>
						<span style="font-weight: normal">{{if .Branches}}[
							{{range $i, $b := .Branches}}
								{{if $i}},{{end}}
								<span class="label label-default">{{$b}}</span>
							{{end}}
						]{{end}}</span>
						{{if .DuplicateID}}<a class="label label-dup" href="#{{.DuplicateID}}">Duplicate result</a>{{end}}
					</th>
				</tr>
			</thead>
			{{if not .DuplicateID}}
			<tbody>
				{{range .Matches}}
				<tr>
					<td>
						<pre class="inline-pre">{{if .URL}}<a href="{{.URL}}" style="color:rgb(135, 162, 185)">{{end}}{{.LineNum}}{{if .URL}}</a>{{end}}</pre>
					</td>
					<td>
						<pre class="inline-pre">{{range .Fragments}}{{LimitPre 100 .Pre}}<b>{{.Match}}</b>{{LimitPost 100 .Post}}{{end}}</pre>
					</td>
				</tr>
				{{end}}
			</tbody>
			{{end}}
		</table>
		{{end}}

	<nav class="navbar navbar-default navbar-bottom">
		<div class="container" style="color:#f0f0f0">
			<p class="navbar-text navbar-right">
			Took {{.Stats.Duration}}{{if .Stats.Wait}}(queued: {{.Stats.Wait}}){{end}} for
			{{HumanUnit .Stats.IndexBytesLoaded}}B index data,
			{{.Stats.NgramMatches}} ngram matches,
			{{.Stats.FilesConsidered}} docs considered,
			{{.Stats.FilesLoaded}} docs ({{HumanUnit .Stats.ContentBytesLoaded}}B)
			loaded{{if or .Stats.FilesSkipped .Stats.ShardsSkipped}},
			{{.Stats.FilesSkipped}} docs and {{.Stats.ShardsSkipped}} shards skipped{{else}}.{{end}}
			</p>
		</div>
	</nav>
	</div>
	{{ template "jsdep"}}
</body>
</html>
`,

	"repolist": `
<html>
{{template "head"}}
<body id="results">
	<div class="container">
		{{template "navbar" .Last}}
		<div><b>
		Found {{.Stats.Repos}} repositories ({{.Stats.Documents}} files, {{HumanUnit .Stats.ContentBytes}}b content)
		</b></div>
		<table>
			<thead>
	<tr>
		<th>Name <a href="/search?q={{.Last.Query}}&order=name">▼</a><a href="/search?q={{.Last.Query}}&order=revname">▲</a></th>
		<th>Last updated <a href="/search?q={{.Last.Query}}&order=revtime">▼</a><a href="/search?q={{.Last.Query}}&order=time">▲</a></th>
		<th>Branches</th>
		<th>Size <a href="/search?q={{.Last.Query}}&order=revsize">▼</a><a href="/search?q={{.Last.Query}}&order=size">▲</a></th>
	</tr>
			</thead>
			<tbody>
	{{range .Repos}}
	<tr>
		<td>{{if .URL}}<a href="{{.URL}}">{{end}}{{.Name}}{{if .URL}}</a>{{end}}</td>
		<td><small>{{.IndexTime.Format "Jan 02, 2006 15:04"}}</small></td>
		<td style="vertical-align: middle;">
			{{range .Branches}}
			{{if .URL}}<tt><a class="label label-default small" href="{{.URL}}">{{end}}{{.Name}}{{if .URL}}</a> </tt>{{end}}&nbsp;
			{{end}}
		</td>
		<td><small>{{HumanUnit .Files}} files ({{HumanUnit .Size}})</small></td>
	</tr>
	{{end}}
			</tbody>
		</table>
	</div>

	<nav class="navbar navbar-default navbar-bottom">
		<div class="container">
			<p class="navbar-text navbar-right">
			</p>
		</div>
	</nav>

	{{ template "jsdep"}}
</body>
</html>
`,

	"print": `
<html>
	{{template "head"}}
	<title>{{.Repo}}:{{.Name}}</title>
<body id="results">
	{{template "navbar" .Last}}
	<div class="container-fluid container-results" >
		 <div class="table table-hover table-condensed" style="overflow:auto; background: #eef;">
			 {{ range $index, $ln := .Lines}}
	 <pre id="l{{Inc $index}}" class="inline-pre"><span class="noselect"><a href="#l{{Inc $index}}">{{Inc $index}}</a>: </span>{{$ln}}</pre>
			 {{end}}
		 </div>
	<nav class="navbar navbar-default navbar-bottom">
		<div class="container">
			<p class="navbar-text navbar-right">
			</p>
		</div>
	</nav>
	</div>
 {{ template "jsdep"}}
</body>
</html>
`,
	"about": `
<html>
	{{template "head"}}
	<title>About ZeroSearch</title>
<body>
<a href="/">ZeroSearch</a> is a code search engine based on the open source project <a href="https://github.com/google/zoekt">Zoekt</a> by <a href="https://github.com/hanwen">Han-Wen Nienhuys</a> et al.
The modifications and ops for ZeroSearch are by Issac Trotts (ijt@zerosearch.io).
Zerosearch.io keeps an index of the <a href="/search?q=r%3A">top few hundred GitHub repositories</a>.
</body>
</html>
`,
}

func init() {
	for k, v := range TemplateText {
		_, err := Top.New(k).Parse(v)
		if err != nil {
			log.Panicf("parse(%s): %v:", k, err)
		}
	}
}
